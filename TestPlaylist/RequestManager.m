//
//  RequestManager.m
//  TestPlaylist
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "RequestManager.h"
#import "AFNetworking.h"

@implementation RequestManager

+ (id) manager {
    static RequestManager *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void) cancelAllRequests {
    if ( [AFHTTPRequestOperationManager manager].operationQueue.operations.count > 0 ) {
        [[AFHTTPRequestOperationManager manager].operationQueue cancelAllOperations];
        [[AFHTTPRequestOperationManager manager].operationQueue waitUntilAllOperationsAreFinished];
    }
}

- (void) getProductsWithLimit:(NSUInteger)limit
                    andOffset:(NSUInteger)offset
                      success: (void (^)(ProductsList* result))successBlock
                        error: (void (^)(NSError *error))errorBlock {
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:@{ @"term": @"jack",
                                                                                    @"entity": @"song",
                                                                                    @"limit": @(limit),
                                                                                    @"offset": @(offset)
                                                                                    }];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:@"https://itunes.apple.com/search" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        ProductsList * productData = [[ProductsList alloc] initWithDictionary:responseObject error:&error];
        if ( !error ) {
            successBlock( productData );
        }
        else {
            successBlock( nil );
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock ( error );
    }];
}

@end
