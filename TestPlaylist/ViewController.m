//
//  ViewController.m
//  TestPlaylist
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import "ViewController.h"
#import "RequestManager.h"
#import "SongTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIScrollView+SVInfiniteScrolling.h"

const NSUInteger songsLimit = 25;

@interface ViewController () {
    BOOL alertVisible;
    BOOL loadMoreStatus;
}

@property (strong) UIAlertView *networkAlertView;
@property (strong) NSMutableArray * productsArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    alertVisible = NO;
    loadMoreStatus = NO;
    
    _productsArray = [NSMutableArray new];
    self.tableView.tableFooterView.hidden = YES;
    
    __weak ViewController * weakself = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        __strong __typeof(weakself)myself = weakself;
        [myself loadDataWithOffset:myself.productsArray.count];
    }];
    
    [self.tableView triggerInfiniteScrolling];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
}

- (void) loadDataWithOffset:(NSUInteger)offset {
    __weak ViewController * weakself = self;
    
    [[RequestManager manager] getProductsWithLimit:songsLimit
                                         andOffset:offset success:^(ProductsList *result) {
                                             __strong __typeof(weakself)myself = weakself;
                                             
                                             if ( [result.results count] > 0 ) {
                                                 [myself.productsArray addObjectsFromArray:result.results];
                                                 [myself refresh];
                                             }
                                             [myself.tableView.infiniteScrollingView stopAnimating];
                                             
                                         } error:^(NSError *error) {
                                             __strong __typeof(weakself)myself = weakself;
                                             
                                             [myself showErrorView:error];
                                             myself.tableView.showsInfiniteScrolling = NO;
                                         }
     ];
}

- (void) showErrorView:(NSError *)error {
    if ( !alertVisible ) {
        alertVisible = YES;
        if ( !_networkAlertView ) {
            _networkAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                           message:[error localizedDescription]
                                                          delegate:nil
                                                 cancelButtonTitle:@"ОK"
                                                 otherButtonTitles:nil];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [_networkAlertView show];
        });
    }
}

- (void) refresh {
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.productsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SongCell";
    SongTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    ProductInfo *info = self.productsArray[indexPath.row];
    
    [cell.albumImage sd_setImageWithURL:[NSURL URLWithString:info.artworkUrl100]
                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                               options:SDWebImageRetryFailed];
    cell.ArtistNameLabel.text = info.artistName;
    cell.SongNameLabel.text = info.trackName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

@end

