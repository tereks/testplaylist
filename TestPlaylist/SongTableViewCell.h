//
//  SongTableViewCell.h
//  TestPlaylist
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SongTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *albumImage;
@property (weak, nonatomic) IBOutlet UILabel *ArtistNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *SongNameLabel;

@end
