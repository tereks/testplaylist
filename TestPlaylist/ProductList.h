//
//  ProductList.h
//  TestPlaylist
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol ProductInfo;

@interface ProductInfo : JSONModel

@property (strong, nonatomic) NSString* wrapperType;
@property (strong, nonatomic) NSString* kind;
@property (strong, nonatomic) NSNumber *artistId;
@property (strong, nonatomic) NSString* artistName;
@property (strong, nonatomic) NSString* collectionName;
@property (strong, nonatomic) NSString* trackName;
@property (strong, nonatomic) NSString* collectionCensoredName;
@property (strong, nonatomic) NSString* trackCensoredName;
@property (strong, nonatomic) NSString* artistViewUrl;
@property (strong, nonatomic) NSString* collectionViewUrl;
@property (strong, nonatomic) NSString* trackViewUrl;
@property (strong, nonatomic) NSString* previewUrl;
@property (strong, nonatomic) NSString* artworkUrl30;
@property (strong, nonatomic) NSString* artworkUrl60;
@property (strong, nonatomic) NSString* artworkUrl100;
@property (strong, nonatomic) NSNumber* isStreamable;

@end

@interface ProductsList : JSONModel

@property (strong, nonatomic) NSArray<ProductInfo, Optional>* results;
@property (strong, nonatomic) NSNumber<Optional> * resultCount;

@end
