//
//  RequestManager.h
//  TestPlaylist
//
//  Created by Sergey Kim on 03.08.15.
//  Copyright (c) 2015 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductList.h"

@interface RequestManager : NSObject

+ (id) manager;

- (void) cancelAllRequests;

- (void) getProductsWithLimit:(NSUInteger)limit
                    andOffset:(NSUInteger)offset
                      success: (void (^)(ProductsList* result))successBlock
                        error: (void (^)(NSError *error))errorBlock;

@end
